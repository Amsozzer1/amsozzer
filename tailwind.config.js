/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        
      },
      backgroundColor: theme => ({
        ...theme('colors'),
        'BODY': '#F6EEE0',
        'container': '#E4B7A0',
        'body-color': '#494949',
        'body-conatiner-light': '#638aae',
        'body-conatiner-dark': '#1c3455',
        'body-conatiner-white': '#ffffff',
      }),
    },
    


  },
  plugins: [],
}

