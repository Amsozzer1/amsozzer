import './App.css';
// import {Projects} from './projects'
import { LandingPage } from './landing_page';
import { BLOGS } from './blogs/BLOGS';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
// import Link from './firebase/LinkedIn'
const router = createBrowserRouter([
  {
    path: "/",
    element: <LandingPage/>,
  },
  {
    path: '/blogs/:id',
    element: <BLOGS/>



  }
  

]);
function App() {
  return (
    <RouterProvider router={router} />
  );
}

export default App;

// function App() {
//   return (

//       <div>
        
//         {/* <Projects/> */}
//       </div>
      

//   );
// }

// export default App;
