import React from "react";
import { useParams } from "react-router-dom";
import  GitBasicsArticle  from "./git-basics";
import ReactHooksBasicsArticle from './React-hooks-basics';
import {ReachOutBox} from './../Reach_ou_box';
import { Footer } from './../footer';
import { Menubar } from './../Menu_bar';
const IDs = {'git-basics': <GitBasicsArticle />,
                'react-hooks_Ahmed': <ReactHooksBasicsArticle />
};
export function BLOGS() {
    let { id } = useParams();
    function getArticle(id) {
        const article = IDs[id];
        if (article) {
            return article;
        }
        return <h1>Article not found</h1>;
    }

  return (
    <div>
    <Menubar />
    {getArticle(id)? getArticle(id): <h1>Article not found</h1>}
  <div className='flex flex-col items-center h-full pt-5' style={{backgroundColor: '#E9E1D6'}}>
  <ReachOutBox/>
  <Footer/>
</div>
</div>
  );
}